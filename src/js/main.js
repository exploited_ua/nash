//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= imagesloaded.pkgd.min.js
//= anime.min.js
//= tiltfx.js

var tiltSettings = [

];

function init() {

    [].slice.call(document.querySelectorAll('a.tilter')).forEach(function (el) {

        new TiltFx(el, tiltSettings[0]);
    });
}

(function () {

    init();
})();


//initialization slide
var swiper = new Swiper('.works-slider', {
    slidesPerView: '3',
    pagination: '.slider-pagination',
    paginationClickable: true,
    nextButton: '.slider-button-next',
    prevButton: '.slider-button-prev',
    spaceBetween: 53,
    autoplay: 5000,
    loop: false,
    onSlideChangeEnd: function (swiper) {
        init();
    },
    breakpoints: {
        992: {
            spaceBetween: 20
        },
        767: {
            slidesPerView: 'auto',
            spaceBetween: 0
        }
    }
});

$('.nav-btn').on('click', function (event) {
    if (this.hash !== "") {
        event.preventDefault();
        $('.section').removeClass('section-current');
        $('#page-wrap').removeClass();
        var hash = this.hash;
        $('#page-wrap').addClass('current-'+hash.substring(1));
        $(hash).addClass('section-current');
        setTimeout(function () {
            window.location.hash = hash;
        },1000);
    }

});
$(document).ready(function () {

    function getLocation() {
        var hashLocation = window.location.hash;
        if(hashLocation !== ''){
            $('.section').removeClass('section-current');
            $('#page-wrap').removeClass();
            $('#page-wrap').addClass('current-'+hashLocation.substring(1));
            $(hashLocation).addClass('section-current');
        }
    }
    getLocation();
    $('#page-wrap').scrollLeft(0);
});


function centerHeightWindow() {
    var heightWindow = $(window).height();
    $('.nav-btn-left , .nav-btn-right').css('top', heightWindow / 2);
}

centerHeightWindow();
$(window).on('resize', function () {
    centerHeightWindow();
});

$('.show-logo').on('mouseenter', function () {
    $(this).mousemove(function () {
        $('.small-logo').fadeIn().css({
            'bottom': '20px',
            'left': '30px'
        });
    });
});

$('.show-logo').on('mouseleave', function () {
    $('.small-logo').fadeOut();
});

$('.show-logo').on('click', function (e) {
    e.preventDefault();
});

$(window).on('load', function () {
    var $preloader = $('.preloader'),
        $spinner   = $preloader.find('#logo');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});